package com.iostreams;

import com.iostreams.services.FileServices;
import com.iostreams.wrappers.BufferedReaderWrapper;
import com.iostreams.wrappers.FileWriterWrapper;

public class Main {

    /*                                       Задание.
        Выполнить вычитку текста из файлов с заданными именами (количество файлов >= 1 и <= Byte.Max).
        Записать считанные данные в другой файл.  Должно быть предусмотрено 3 режима записи:
        Стереть все существующие данные и записать новые данные.
        Записать данные в начало файла. Существующие данные не затираются.
        Записать данные в конец файла. Существующие данные не затираются. */

    public static void main(String[] args) {
        FileServices fileServices = new FileServices(new BufferedReaderWrapper(), new FileWriterWrapper());
        String[] fileNames = {"DataStore\\old_file_1.txt",
        "DataStore\\old_file_2.txt",
        "DataStore\\old_file_3.txt"};

        String name = "DataStore/new_file_2.txt";

        String[] n = {name};

        fileServices.readFiles(n);
    }
}
