package com.iostreams.services;

import com.iostreams.wrappers.BufferedReaderWrapper;
import com.iostreams.wrappers.FileWriterWrapper;

import java.io.*;
import java.util.ArrayList;

public class FileServices {


    private FileWriterWrapper writerWrapper;
    private BufferedReaderWrapper readerWrapper;

    private static final String STRING_TO_APPEND_IS_EMPTY = "";

    public FileServices(BufferedReaderWrapper readerWrapper, FileWriterWrapper writerWrapper) {
        this.readerWrapper = readerWrapper;
        this.writerWrapper = writerWrapper;
    }

    public String readFiles(String[] fileNames) {
        if(fileNames == null){
            return null;
        }
        int arrayLength = fileNames.length;
        if (arrayLength < 1 || arrayLength > Byte.MAX_VALUE) {
            return null;
        }
        ArrayList<File> files = new ArrayList<>();
        for (int i = 0; i < fileNames.length; i++) {
            if (fileNames[i] != null) {
                files.add(new File(fileNames[i]));
            }
        }
        String allText = STRING_TO_APPEND_IS_EMPTY;
        for (File file : files) {
            String fileText = readFile(file);
            if (fileText != null&&allText.length() !=0){
                fileText = "\n"+fileText;
            }
            allText += fileText;
        }
        return allText;
    }

    private String readFile(File file) {
        String fileText = STRING_TO_APPEND_IS_EMPTY;

        try(BufferedReader reader = readerWrapper.getBufferedReader(file)) {
            String fileString = reader.readLine();

            while (fileString != null) {
                fileText += fileString;
                fileText += "\n";
                fileString = reader.readLine();
            }
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        if (fileText.length()>0){
        fileText = fileText.substring(0, fileText.length() - 1);
        }

        return fileText;
    }

    public void writeFileDelete(String text, String fileName) {
        if (fileName == null&&text == null) {
            return;
        }
        File file = new File(fileName);
        try (FileWriter writer = writerWrapper.getFileWriter(file, false)) {
            writer.write(text);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public  void writeFileStart(String text, String fileName) {
        if (fileName == null||text == null){
            return;
        }
        File file = new File(fileName);
        String fileText = readFile(file);
        if (fileText.length() > 0){
            text += "\n"+fileText;
        }
        try (FileWriter writer = writerWrapper.getFileWriter(file, false)) {
            writer.write(text);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void writeFileEnd(String text, String fileName) {
        if (fileName == null||text == null){
            return;
        }
        File file = new File(fileName);
        if (file.length() != 0) {
            text = "\n" + text;
        }
        try (FileWriter writer = writerWrapper.getFileWriter(file, true)) {
            writer.write(text);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
