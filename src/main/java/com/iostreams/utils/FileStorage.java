package com.iostreams.utils;

import java.io.File;
import java.io.IOException;

public class FileStorage {

    public static final File DIRECTORY;

    public static final File OLD_FILE_1;

    public static final File OLD_FILE_2;

    public static final File OLD_FILE_3;

    public static final File NEW_FILE_1;

    public static final File NEW_FILE_2;

    public static final File NEW_FILE_3;

    static {
        DIRECTORY = new File("DataStore");

        if (!DIRECTORY.exists()) {
            DIRECTORY.mkdir();
        }

        OLD_FILE_1 = new File("DataStore\\old_file_1.txt");

        OLD_FILE_2 = new File("DataStore\\old_file_2.txt");

        OLD_FILE_3 = new File("DataStore\\old_file_3.txt");

        NEW_FILE_1 = new File("DataStore\\new_file_1.txt");

        NEW_FILE_2 = new File("DataStore\\new_file_2.txt");

        NEW_FILE_3 = new File("DataStore\\new_file_3.txt");

        try{
            if (!OLD_FILE_1.exists()){
                OLD_FILE_1.createNewFile();
            }
        } catch (IOException e){
            e.printStackTrace();
        }

        try{
            if (!OLD_FILE_2.exists()){
                OLD_FILE_2.createNewFile();
            }
        } catch (IOException e){
            e.printStackTrace();
        }

        try{
            if (!OLD_FILE_3.exists()){
                OLD_FILE_3.createNewFile();
            }
        } catch (IOException e){
            e.printStackTrace();
        }

        try{
            if (!NEW_FILE_1.exists()){
                NEW_FILE_1.createNewFile();
            }
        } catch (IOException e){
            e.printStackTrace();
        }

        try{
            if (!NEW_FILE_2.exists()){
                NEW_FILE_2.createNewFile();
            }
        } catch (IOException e){
            e.printStackTrace();
        }

        try{
            if (!NEW_FILE_3.exists()){
                NEW_FILE_3.createNewFile();
            }
        } catch (IOException e){
            e.printStackTrace();
        }
    }
}