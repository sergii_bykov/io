package services;

import com.iostreams.services.FileServices;
import com.iostreams.wrappers.BufferedReaderWrapper;
import com.iostreams.wrappers.FileWriterWrapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;

import java.io.*;

public class FileServicesTest {
    private static final String NEW_FILE = "DataStore/new_file_1.txt";
    private static final String NEW_FILE2 = "DataStore/new_file_2.txt";
    private static final String OLD_FILE = "DataStore/old_file_1.txt";

    BufferedReaderWrapper readerWrapper = Mockito.mock(BufferedReaderWrapper.class);
    FileWriterWrapper writerWrapper = Mockito.mock(FileWriterWrapper.class);

    FileWriter fileWriter = Mockito.mock(FileWriter.class);
    FileWriter fileWriterToEnd = Mockito.mock(FileWriter.class);

    BufferedReader bufferedReader = Mockito.mock(BufferedReader.class);
    BufferedReader bufferedReader2 = Mockito.mock(BufferedReader.class);

    FileServices cut = new FileServices(readerWrapper, writerWrapper);

    static Arguments[] readFilesTestArgs(){
        return new Arguments[]{
                Arguments.arguments("abc123", new String[]{NEW_FILE}, new File(NEW_FILE), null, "abc123", null, null, null, null),
                Arguments.arguments("abc123\n456\n789", new String[]{NEW_FILE}, new File(NEW_FILE), null, "abc123", "456", "789", null, null),
                Arguments.arguments("java\nhello\nworld\nhard\nversion", new String[]{NEW_FILE, OLD_FILE}, new File(NEW_FILE),
                                    new File(OLD_FILE), "java", "hello", "world", "hard", "version"),
                Arguments.arguments(null, null, new File(NEW_FILE), null, "abc123", "456", "789", null, null),
                Arguments.arguments(null, new String[]{}, new File(NEW_FILE), null, "abc123", "456", "789", null, null),
                Arguments.arguments(null, new String[]{
                                NEW_FILE, OLD_FILE, NEW_FILE, OLD_FILE, NEW_FILE, OLD_FILE,OLD_FILE, NEW_FILE, OLD_FILE, OLD_FILE,
                                NEW_FILE, OLD_FILE, NEW_FILE, OLD_FILE, NEW_FILE, OLD_FILE,OLD_FILE, NEW_FILE, OLD_FILE, OLD_FILE,
                                NEW_FILE, OLD_FILE, NEW_FILE, OLD_FILE, NEW_FILE, OLD_FILE,OLD_FILE, NEW_FILE, OLD_FILE, OLD_FILE,
                                NEW_FILE, OLD_FILE, NEW_FILE, OLD_FILE, NEW_FILE, OLD_FILE,OLD_FILE, NEW_FILE, OLD_FILE, OLD_FILE,
                                NEW_FILE, OLD_FILE, NEW_FILE, OLD_FILE, NEW_FILE, OLD_FILE,OLD_FILE, NEW_FILE, OLD_FILE, OLD_FILE,
                                NEW_FILE, OLD_FILE, NEW_FILE, OLD_FILE, NEW_FILE, OLD_FILE,OLD_FILE, NEW_FILE, OLD_FILE, OLD_FILE,
                                NEW_FILE, OLD_FILE, NEW_FILE, OLD_FILE, NEW_FILE, OLD_FILE,OLD_FILE, NEW_FILE, OLD_FILE, OLD_FILE,
                                NEW_FILE, OLD_FILE, NEW_FILE, OLD_FILE, NEW_FILE, OLD_FILE,OLD_FILE, NEW_FILE, OLD_FILE, OLD_FILE,
                                NEW_FILE, OLD_FILE, NEW_FILE, OLD_FILE, NEW_FILE, OLD_FILE,OLD_FILE, NEW_FILE, OLD_FILE, OLD_FILE,
                                NEW_FILE, OLD_FILE, NEW_FILE, OLD_FILE, NEW_FILE, OLD_FILE,OLD_FILE, NEW_FILE, OLD_FILE, OLD_FILE,
                                NEW_FILE, OLD_FILE, NEW_FILE, OLD_FILE, NEW_FILE, OLD_FILE,OLD_FILE, NEW_FILE, OLD_FILE, OLD_FILE,
                                NEW_FILE, OLD_FILE, NEW_FILE, OLD_FILE, NEW_FILE, OLD_FILE,OLD_FILE, NEW_FILE, OLD_FILE, OLD_FILE,
                                NEW_FILE, OLD_FILE, NEW_FILE, OLD_FILE, NEW_FILE, OLD_FILE,OLD_FILE, NEW_FILE
                        },
                        new File(NEW_FILE), null, "128 >", "Byte.Max", "must", "be", "null"),
                Arguments.arguments("abc123", new String[]{NEW_FILE, null}, new File(NEW_FILE), null, "abc123", null, null, null, null),

        };
    }

    @ParameterizedTest
    @MethodSource("readFilesTestArgs")
    void readFilesTest(String expected, String[] fileNames, File file1, File file2, String text1, String text2, String text3, String text4, String text5) throws IOException {
        Mockito.when(readerWrapper.getBufferedReader(file1)).thenReturn(bufferedReader);
        Mockito.when(bufferedReader.readLine()).thenReturn(text1).thenReturn(text2).thenReturn(text3).thenReturn(null);
        Mockito.when(readerWrapper.getBufferedReader(file2)).thenReturn(bufferedReader2);
        Mockito.when(bufferedReader2.readLine()).thenReturn(text4).thenReturn(text5).thenReturn(null);

        String actual = cut.readFiles(fileNames);
        Assertions.assertEquals(expected, actual);
    }

    static Arguments[] writeFileWithDeleteTestArgs(){
        return new Arguments[]{
                Arguments.arguments("12345", NEW_FILE, new File(NEW_FILE), 1)

        };
    }

    @ParameterizedTest
    @MethodSource("writeFileWithDeleteTestArgs")
    void writeFileWithDeleteTest(String text, String fileName, File file, int times) throws IOException {
        Mockito.when(writerWrapper.getFileWriter(file, false)).thenReturn(fileWriter);
        cut.writeFileDelete(text, fileName);
        Mockito.verify(fileWriter, Mockito.times(times)).write(text);
    }

    static Arguments[] writeFileToEndTestArgs(){
        return new Arguments[]{
                Arguments.arguments("12345", "12345", NEW_FILE2, new File(NEW_FILE2), 1),
                Arguments.arguments("12345", "\n12345", NEW_FILE, new File(NEW_FILE), 1)

        };
    }

    @ParameterizedTest
    @MethodSource("writeFileToEndTestArgs")
    void writeFileToEndTest(String text, String textToWrite, String fileName, File file, int times) throws IOException {
        Mockito.when(writerWrapper.getFileWriter(file, true)).thenReturn(fileWriterToEnd);
        cut.writeFileEnd(text, fileName);
        Mockito.verify(fileWriterToEnd, Mockito.times(times)).write(textToWrite);
    }

    static Arguments[] writeFileToStartTestArgs(){
        return new Arguments[]{
                Arguments.arguments("12345", "6789", "10", "11", "12345\n6789\n10\n11", NEW_FILE2, new File(NEW_FILE2), 1),
                Arguments.arguments("12345", null, null, null, "12345", NEW_FILE, new File(NEW_FILE), 1),
                
        };
    }

    @ParameterizedTest
    @MethodSource("writeFileToStartTestArgs")
    void writeFileToStartTest(String text, String textInFileLine1, String textInFileLine2, String textInFileLine3, String textToWrite, String fileName, File file, int times) throws IOException {
        Mockito.when(readerWrapper.getBufferedReader(file)).thenReturn(bufferedReader);
        Mockito.when(bufferedReader.readLine()).thenReturn(textInFileLine1).thenReturn(textInFileLine2).thenReturn(textInFileLine3).thenReturn(null);

        Mockito.when(writerWrapper.getFileWriter(file, false)).thenReturn(fileWriterToEnd);
        cut.writeFileStart(text, fileName);
        Mockito.verify(fileWriterToEnd, Mockito.times(times)).write(textToWrite);
    }

}
